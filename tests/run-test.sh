#!/bin/sh
set -eux

cd $SOURCE
git init
make bots test/common machine

# only install a subset to save time/space
mv package.json .package.json
npm install chrome-remote-interface sizzle
mv .package.json package.json

. /etc/os-release
export TEST_OS="${ID}-${VERSION_ID/./-}"

RC=0

for t in test/verify/check-{blueprint,custom,dependence,package,service,source}
do
  $t \
    --enable-network \
    --machine 127.0.0.1:22 \
    --browser 127.0.0.1:9090 \
    --verbose --trace \
    || RC=$?
done

echo $RC > "$LOGS/exitcode"
cp --verbose Test* "$LOGS" || true
# deliver test result via exitcode file
exit 0
