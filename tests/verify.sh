#!/bin/sh
set -eux

source /etc/os-release
ARCH=$(uname -m)
# from standard-test-source
SOURCE="$(pwd)/source"
LOGS="$(pwd)/logs"
TESTS="$(pwd)/tests"
mkdir -p "$LOGS"
chmod a+w "$LOGS"

# This section is taken from https://github.com/osbuild/osbuild-composer/blob/main/schutzbot/mockbuild.sh
# chromium-headless is only available in EPEL for RHEL.
if [[ $ID == rhel || $ID == centos ]] && [[ ${VERSION_ID%.*} == 8 ]] && ! rpm -q epel-release; then
    dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
    dnf config-manager --enable epel
elif [[ $ID == rhel || $ID == centos ]] && [[ ${VERSION_ID%.*} == 9 ]]; then
    dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
    # replace the releasever 9 with 8 so that the epel repos can be found.
    # taken from https://github.com/cockpit-project/cockpit
    sed -i 's/$releasever/8/' /etc/yum.repos.d/epel*.repo
    dnf config-manager --enable epel
fi
# HACK: chromium-headless ought to be enough, but greater than version 88 has a crash: https://bugs.chromium.org/p/chromium/issues/detail?id=1170634
dnf install -y chromium

# Taken from https://github.com/cockpit-project/cockpit-machines/commit/6206acd32cb125d54cb17be74afb60d45504e99f#diff-8bc7fea4b1a7acd44c6a752365847e961fa72cbb68efa39ed77ca406e9a5cac4
#HACK: unbreak rhel-9-0's default choice of 999999999 rounds, see https://bugzilla.redhat.com/show_bug.cgi?id=1993919
sed -ie 's/#SHA_CRYPT_MAX_ROUNDS 5000/SHA_CRYPT_MAX_ROUNDS 5000/' /etc/login.defs

# overriding osbuild-composer repo with nightly
mkdir -p /etc/osbuild-composer/repositories
cp $SOURCE/test/files/rhel-90.json /etc/osbuild-composer/repositories/rhel-90.json

# Make cockpit.socket and lorax-composer start
systemctl start cockpit.socket
systemctl start osbuild-composer.socket

# create user account for logging in
if ! id admin 2>/dev/null; then
    useradd -c Administrator -G wheel admin
    echo admin:foobar | chpasswd
fi

# create user account for running the test
if ! id runtest 2>/dev/null; then
    useradd -c 'Test runner' runtest
    # allow test to set up things on the machine
    mkdir -p /root/.ssh
    curl https://raw.githubusercontent.com/cockpit-project/bots/master/machine/identity.pub  >> /root/.ssh/authorized_keys
    chmod 600 /root/.ssh/authorized_keys
fi
chown -R runtest "$SOURCE"

# disable core dumps, we rather investigate them upstream where test VMs are accessible
echo core > /proc/sys/kernel/core_pattern

cp -r $SOURCE/test/files /home/admin

# Run tests as unprivileged user
su - -c "env SOURCE=$SOURCE LOGS=$LOGS $TESTS/run-test.sh" runtest

RC=$(cat $LOGS/exitcode)
exit ${RC:-1}
